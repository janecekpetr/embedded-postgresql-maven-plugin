## Description

This is a fork of [slavaz/embedded-postgresql-maven-plugin] where the underlying embedded Postgres provider library
had been switched from [yandex-qatools/postgresql-embedded] to [opentable/otj-pg-embedded] because of its superior
compatibility with Windows.

Embedded PostgreSQL Maven Plugin provides a platform neutral way for running Postgres server locally, without Postgres
actually being installed, be it for integration tests or any other part of your Maven build.

### Differences so far

- Runs on Windows even when you're a privileged user.
- A fail in the process initialization/startup now means a failed build.
- The plugin can be run multiple times during a single Maven build. Currently not at the same time, though.
- Locale, charset and Postgres version configuration options have been removed for now.

### TODO

- [ ] locale and charset configuration options
- [ ] possibility to run 2+ servers at the same time
- [ ] modularize the upstream lib to support any Postgres version ([otj-pg-embedded/issues/28])
- [ ] thread safety

## Goals
                  
1. `start` - Start the server.
2. `stop` - Stop the server. This is an optional goal, the server will be torn down upon JVM shutdown anyway.

## Configuration

### ~~pgServerVersion~~
~~The plugin currently supports next PostgreSQL versions: 9.4.10, 9.5.5, 9.6.1~~

~~You also may use aliases:~~
* ~~9.4 -> 9.4.10~~
* ~~9.5 -> 9.5.5~~
* ~~9.6 -> 9.6.2~~
* ~~latest -> 9.6~~

### pgDatabaseDir

Where server files will be plased. Defaults to  `{project.build.directory}/pgdata`.

### pgPort

Port for listening incoming connections.

### pgLocale

~~Locale for embedded PostgreSQL server. Leave empty for running the server with system locale.~~
~~Specify "no" to skip locale & charset definition.~~

Always uses the default system locale.

### pgCharset

~~Charset for embedded PostgreSQL server. Leave empty for running the server with system charset.~~
~~Specify "no" to skip locale & charset definition.~~

Always uses the default system charset.

### dbName

Database name, defaults to `postgres`. This will be created if not existing already.

### userName

User name, defaults to the _dbName_. This will be created if not existing already.

### password

User password for the newly created user.

## Usage example

Starts PostgreSQL server, creates a database and a user with specified password

```
<build>
	<plugins>
		<plugin>
			<groupId>cz.slanec</groupId>
			<artifactId>embedded-postgresql-maven-plugin</artifactId>
			<configuration>
				<pgServerPort>15432</pgServerPort>
				<dbName>testdb</dbName>
				<userName>testuser</userName>
				<password>userpass</password>
			</configuration>
			<executions>
				<execution>
					<id>start-pgsql</id>
					<phase>pre-integration-test</phase>
					<goals>
						<goal>start</goal>
					</goals>
				</execution>
				<execution>
					<id>stop-pgsql</id>
					<phase>post-integration-test</phase>
					<goals>
						<goal>stop</goal>
					</goals>
				</execution>
			</executions>
		</plugin>
	</plugins>
</build>
```

[slavaz/embedded-postgresql-maven-plugin]: https://github.com/slavaz/embedded-postgresql-maven-plugin/
[yandex-qatools/postgresql-embedded]: https://github.com/yandex-qatools/postgresql-embedded
[opentable/otj-pg-embedded]: https://github.com/opentable/otj-pg-embedded
[otj-pg-embedded/issues/28]: https://github.com/opentable/otj-pg-embedded/issues/28