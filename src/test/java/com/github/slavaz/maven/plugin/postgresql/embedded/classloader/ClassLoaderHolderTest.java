package com.github.slavaz.maven.plugin.postgresql.embedded.classloader;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.fail;

public class ClassLoaderHolderTest {

    private ClassLoader classLoader;

    private ClassLoader actualClassLoader;

    @BeforeMethod
    public void setUp() throws Exception {
        ClassLoaderHolder.setClassLoader(null);
    }

    @Test
    public void happyPath_setClassLoader() {
        given_aClassLoader();

        when_setClassLoaderCalled();

        then_theClassLoaderShouldBeSet();
    }

    @Test(expectedExceptions = {
            IllegalStateException.class }, expectedExceptionsMessageRegExp = "A classloader has already been set.*")
    public void unhappyPath_setClassLoaderCalledTwice() {
        given_aClassLoader();

        when_setClassLoaderCalledTwice();

        then_anExceptionShouldBeRaised();
    }

    private void when_setClassLoaderCalledTwice() {
        when_setClassLoaderCalled();
        when_setClassLoaderCalled();
    }

    @Test
    public void unhappyPath_noClassLoader() {

        when_getClassLoaderCalled();

        then_emptyClassLoaderShouldBeReturned();
    }

    private void then_emptyClassLoaderShouldBeReturned() {
        assertFalse(actualClassLoader != null);
    }

    private void when_getClassLoaderCalled() {
        actualClassLoader = ClassLoaderHolder.getClassLoader();
    }

    private static void then_anExceptionShouldBeRaised() {
        fail("An exception should be raised");
    }

    private void then_theClassLoaderShouldBeSet() {
        assertSame(ClassLoaderHolder.getClassLoader(), classLoader);
    }

    private void when_setClassLoaderCalled() {
        ClassLoaderHolder.setClassLoader(classLoader);
    }

    private void given_aClassLoader() {
        classLoader = mock(ClassLoader.class);
    }
}