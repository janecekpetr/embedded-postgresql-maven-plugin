package com.github.slavaz.maven.plugin.postgresql.embedded.classloader;

public class ClassLoaderHolder {

    private static volatile ClassLoader classLoader;

    private ClassLoaderHolder() {
    	// a static singleton
    }

    public static void setClassLoader(ClassLoader classLoader) {
        if ((classLoader != null) && (ClassLoaderHolder.classLoader != null)) {
            throw new IllegalStateException("A classloader has already been set. Run the `stop` goal before running start twice.");
        }
        ClassLoaderHolder.classLoader = classLoader;
    }

    public static ClassLoader getClassLoader() {
        return classLoader;
    }
}