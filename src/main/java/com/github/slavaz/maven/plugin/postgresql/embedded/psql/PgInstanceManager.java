package com.github.slavaz.maven.plugin.postgresql.embedded.psql;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Locale;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class PgInstanceManager {
	
	private static EmbeddedPostgres postgres = null;

    public static void start(IPgInstanceProcessData pgInstanceProcessData) throws Exception {
        if (postgres != null) {
            throw new IllegalStateException("Postgres already started");
        }
        
        Files.createDirectories(pgInstanceProcessData.getPgDatabaseDir());
        
        // TODO locale, charset
        postgres = EmbeddedPostgres.builder()
        	.setDataDirectory(pgInstanceProcessData.getPgDatabaseDir().toFile())
        	.setPort(pgInstanceProcessData.getPgPort())
        	.start();
        
//        String userName = pgInstanceProcessData.getUserName();
//        String dbName = pgInstanceProcessData.getDbName();
//        DataSource dataSource = postgres.getDatabase(userName, dbName);
//        try (
//                Connection c = dataSource.getConnection();
//                PreparedStatement stmt = c.prepareStatement(
//                        String.format("CREATE DATABASE %s OWNER %s ENCODING = 'utf8'", dbName, userName))
//            ) {
//            stmt.execute();
//        }
    }

    public static void stop() throws IOException {
        if (postgres == null) {
			return;
		}
        
        postgres.close();
    }

    static class CharsetParametersList {
        private final static String NO_PARAMETERS = "no";

        private final String charsetName;
        private final String localeName;

        CharsetParametersList(final IPgInstanceProcessData pgInstanceProcess) {
            charsetName = calculateCharset(pgInstanceProcess);
            localeName = calculateLocale(pgInstanceProcess);
        }

        Collection<String> get() {
            if (NO_PARAMETERS.equals(localeName) || NO_PARAMETERS.equals(charsetName)) {
                return emptyList();
            }

            final String lc = localeName + "." + charsetName;
            return asList("-E", charsetName, "--locale=" + lc, "--lc-collate=" + lc, "--lc-ctype=" + lc);
        }

        private static String calculateCharset(final IPgInstanceProcessData iPgInstanceProcessData) {
            if (StringUtils.isEmpty(iPgInstanceProcessData.getPgCharset())) {
                return Charset.defaultCharset().name();
            }
            return iPgInstanceProcessData.getPgCharset();
        }

        private static String calculateLocale(final IPgInstanceProcessData iPgInstanceProcessData) {
            if (StringUtils.isEmpty(iPgInstanceProcessData.getPgLocale())) {
                return Locale.getDefault().toString();
            }
            return iPgInstanceProcessData.getPgLocale();
        }
    }
}