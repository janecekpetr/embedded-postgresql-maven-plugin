package com.github.slavaz.maven.plugin.postgresql.embedded.goals;

import com.github.slavaz.maven.plugin.postgresql.embedded.classloader.ClassLoaderHolder;
import com.github.slavaz.maven.plugin.postgresql.embedded.classloader.ClassLoaderUtils;
import com.github.slavaz.maven.plugin.postgresql.embedded.psql.IPgInstanceProcessData;
import com.github.slavaz.maven.plugin.postgresql.embedded.psql.IsolatedPgInstanceManager;
import com.github.slavaz.maven.plugin.postgresql.embedded.psql.data.PgInstanceProcessData;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

@Mojo(name = "start")
public class StartGoalMojo extends AbstractGoalMojo {

    @Parameter(defaultValue = "${project.build.directory}")
    private String projectBuildDir;

//    @Parameter(defaultValue = "latest", property = "pgVersion", required = true)
//    private String pgServerVersion;

    @Parameter(property = "pgDatabasedir", required = false)
    private String pgDatabaseDir;

    @Parameter(property = "dbname", required = true)
    private String dbName;

    @Parameter(defaultValue = "postgres", property = "username", required = true)
    private String userName;

    @Parameter(defaultValue = "postgres", required = true)
    private String password;

//    @Parameter(property = "pgLocale")
//    private String pgLocale;

//    @Parameter(property = "pgCharset")
//    private String pgCharset;

    @Parameter(defaultValue = "5432", property = "pgPort", required = true)
    private int pgServerPort;

    @Parameter(readonly = true, defaultValue = "${plugin.artifacts}")
    private List<Artifact> pluginDependencies;

    @Override
	protected void doExecute() throws MojoExecutionException, MojoFailureException {
        getLog().info("Starting PostgreSQL...");
        calculateDatabaseDir();

        ClassLoader classLoader = ClassLoaderUtils.buildClassLoader(pluginDependencies);
        ClassLoaderHolder.setClassLoader(classLoader);
        new IsolatedPgInstanceManager(classLoader).start(buildInstanceProcessData());

        getLog().info("PostgreSQL started.");
    }

    private void calculateDatabaseDir() {
        if (StringUtils.isEmpty(pgDatabaseDir)) {
            pgDatabaseDir = projectBuildDir + File.separator + "pgdata";
        }
    }

    private IPgInstanceProcessData buildInstanceProcessData() {
        return new PgInstanceProcessData(/*pgServerVersion, */pgServerPort,
                dbName, userName, password, Paths.get(pgDatabaseDir)/*, pgLocale, pgCharset*/);
    }
}