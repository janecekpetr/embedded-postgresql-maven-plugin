package com.github.slavaz.maven.plugin.postgresql.embedded.psql;

import java.nio.file.Path;

public interface IPgInstanceProcessData {
	
    public String getPgServerVersion();
    public void setPgServerVersion(String pgServerVersion);

    public String getDbName();
    public void setDbName(String dbName);

    public String getUserName();
    public void setUserName(String userName);

    public String getPassword();
    public void setPassword(String password);

    public Path getPgDatabaseDir();
    public void setPgDatabaseDir(Path pgDatabaseDir);

    public int getPgPort();
    public void setPgPort(int pgPort);

    public String getPgLocale();
    public void setPgLocale(String pgLocale);

    public String getPgCharset();
    public void setPgCharset(String pgCharset);
}