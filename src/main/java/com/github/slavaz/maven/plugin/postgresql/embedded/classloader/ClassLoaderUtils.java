package com.github.slavaz.maven.plugin.postgresql.embedded.classloader;

import org.apache.commons.lang3.Validate;
import org.apache.maven.artifact.Artifact;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;

public class ClassLoaderUtils {

    private ClassLoaderUtils() {
    	// a static util class
    }

    public static ClassLoader buildClassLoader(List<Artifact> artifacts) {
        Validate.notEmpty(artifacts);
        URL[] urls = artifacts.stream()
            .map(artifact -> artifact.getFile())
            .map(file -> file.toURI())
            .map(uri -> uriToURL(uri))
            .toArray(size -> new URL[size]);
        return new URLClassLoader(urls);
    }

    private static URL uriToURL(URI uri) {
        try {
            return uri.toURL();
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Malformed URL: " + uri, e);
        }
    }
}