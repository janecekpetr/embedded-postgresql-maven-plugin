package com.github.slavaz.maven.plugin.postgresql.embedded.goals;

import com.github.slavaz.maven.plugin.postgresql.embedded.classloader.ClassLoaderHolder;
import com.github.slavaz.maven.plugin.postgresql.embedded.psql.IsolatedPgInstanceManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "stop")
public class StopGoalMojo extends AbstractGoalMojo {
	
    @Override
	protected void doExecute() throws MojoExecutionException, MojoFailureException {
        ClassLoader classLoader = ClassLoaderHolder.getClassLoader();
        if (classLoader != null) {
            try {
                getLog().info("Stopping PostgreSQL...");
                new IsolatedPgInstanceManager(classLoader).stop();
                ClassLoaderHolder.setClassLoader(null);
                getLog().info("PostgreSQL stopped.");
            } catch (Exception e) {
                getLog().error("Failed to stop PostgreSQL", e);
            }
        } else {
            getLog().warn("PostgreSQL was not started");
        }
    }
    
}