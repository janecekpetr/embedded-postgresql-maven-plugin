package com.github.slavaz.maven.plugin.postgresql.embedded.psql.data;

import java.nio.file.Path;

import com.github.slavaz.maven.plugin.postgresql.embedded.psql.IPgInstanceProcessData;

public class PgInstanceProcessData implements IPgInstanceProcessData {
	
    private String pgServerVersion;
    private int pgPort;
    private String dbName;
    private String userName;
    private String password;
    private Path pgDatabaseDir;
    private String pgLocale;
    private String pgCharset;

    public PgInstanceProcessData(String pgServerVersion, int pgPort, String dbName, String userName, String password, Path pgDatabaseDir, String pgLocale, String pgCharset) {
        this.pgServerVersion = pgServerVersion;
        this.pgPort = pgPort;
        this.dbName = dbName;
        this.userName = userName;
        this.password = password;
        this.pgDatabaseDir = pgDatabaseDir;
        this.pgLocale = pgLocale;
        this.pgCharset = pgCharset;
    }

    public PgInstanceProcessData(int pgPort, String dbName, String userName, String password, Path pgDatabaseDir) {
    	this(null, pgPort, dbName, userName, password, pgDatabaseDir, null, null);
	}

	@Override
	public String getPgServerVersion() {
        return pgServerVersion;
    }

    @Override
	public void setPgServerVersion(String pgServerVersion) {
        this.pgServerVersion = pgServerVersion;
    }

    @Override
	public String getDbName() {
        return dbName;
    }

    @Override
	public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    @Override
	public String getUserName() {
        return userName;
    }

    @Override
	public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
	public String getPassword() {
        return password;
    }

    @Override
	public void setPassword(String password) {
        this.password = password;
    }

    @Override
	public Path getPgDatabaseDir() {
        return pgDatabaseDir;
    }

    @Override
	public void setPgDatabaseDir(Path pgDatabaseDir) {
        this.pgDatabaseDir = pgDatabaseDir;
    }

    @Override
	public int getPgPort() {
        return pgPort;
    }

    @Override
	public void setPgPort(int pgPort) {
        this.pgPort = pgPort;
    }

    @Override
	public String getPgLocale() {
        return pgLocale;
    }

    @Override
	public void setPgLocale(String pgLocale) {
        this.pgLocale = pgLocale;
    }

    @Override
	public String getPgCharset() {
        return pgCharset;
    }

    @Override
	public void setPgCharset(String pgCharset) {
        this.pgCharset = pgCharset;
    }
}